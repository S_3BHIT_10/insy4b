<?php
    header('Access-Control-Allow-Origin: *');

    include("./pdo.php");
    if(isset($_GET['m_id'])) {

        $mitgliederid=7;

        $query = 'SELECT m.m_id, m.Vorname, m.Nachname, m.f_id, f.f_id FROM mitglieder m, funktionen f WHERE m.f_id ='.$mitgliederid.' AND f.f_id='.$mitgliederid;
        $res1 = $db->query($query);
        $mitglieder = ($res1->fetchAll(PDO::FETCH_ASSOC));

        $query = 'SELECT m.m_id, m.Vorname, m.Nachname, m.f_id, f.f_id, f.Funktion FROM mitglieder m, funktionen f WHERE f.f_id = m.f_id AND m.f_id < '. $mitgliederid;
        $res2 = $db->query($query);
        $funktionen = ($res2->fetchAll(PDO::FETCH_ASSOC));

        echo '
                    <div id="linkercontainer">
                        <h3>Vorstand</h3>
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Funktion</th>
                            </tr>
                          </thead>
                          <tbody>
            ';

        for ($i = 0; $i < $res2->rowCount(); $i++) {
            echo '
                                <tr>
                                    <th scope="row">' . $funktionen[$i]['m_id'] . '</th>
                                    <td>' . $funktionen[$i]['Funktion'] . '</td>
                                    <td>' . $funktionen[$i]['Vorname'] . ' ' . $funktionen[$i]['Nachname'] . '</td>
                                </tr>
            ';
        }

        echo '
                            </tbody>
                        </table>
                    </div>
                    <div id="rechtercontainer">
                        <h3>Mitglieder</h3>
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                            </tr>
                          </thead>
                          <tbody>
        ';

        for ($i = 0; $i < $res1->rowCount(); $i++) {
            echo '
                                <tr>
                                    <th scope="row">' . $mitglieder[$i]['m_id'] . '</th>
                                    <td>' . $mitglieder[$i]['Vorname'] . ' ' . $mitglieder[$i]['Nachname'] . '</td>
                                </tr>
            ';
        }

        echo '
                            </tbody>
                        </table>
                    </div>
        ';
    }
?>
