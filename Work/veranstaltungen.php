<?php
header('Access-Control-Allow-Origin: *');
include("./pdo.php");
if(isset($_GET['m_id'])&&isset($_GET['adminrechte']))
{
    $query='SELECT v_id FROM beantwVeranstaltungen WHERE m_id ='.$_GET['m_id'];
    $res1=$db->query($query);
    $array_v_id=($res1->fetchAll(PDO::FETCH_ASSOC));
    $uids="0";
    for($i=0; $i < $res1->rowCount(); $i++)
    {
        $zwischenerg = implode(",",$array_v_id[$i]);
        $uids=$uids.",".$zwischenerg;
    }
    $query='SELECT v.v_id, v.veranstname, v.veranstaltungsdatum, v.veranstalter, v.teilnehmer, m.m_id, m.Vorname, m.Nachname FROM veranstaltungen v, mitglieder m WHERE v.m_id = m.m_id AND v.v_id NOT IN('.$uids.') ORDER BY v.erstellerdatum DESC';
    $res1=$db->query($query);
    if(($res1->rowCount())>0)
    {
        $veranstaltungen=($res1->fetchAll(PDO::FETCH_ASSOC));
        echo '
                <style type="text/css">
                    td{
                        text-align:center;
                        vertical-align: middle;
                    }
                    th{
                        text-align:center;
                        vertical-align: middle;
                    }
                </style>
                <table class="table table-striped" id="umfragentable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Datum</th>
                            <th>Herausgeber</th>
                            <th>Veranstalter</th>
                            <th>Teilnehmer</th>
                            <th>Zusagen</th>
                            <th>Absagen</th>
                        </tr>
                    </thead>
                    <tbody>
            ';
        for($i=0; $i<$res1->rowCount(); $i++)
        {
            echo '
                        <tr data-vid="'.$veranstaltungen[$i]['v_id'].'">
                            <td>'.$veranstaltungen[$i]['veranstname'].'</td>
                            <td>'.$veranstaltungen[$i]['veranstaltungsdatum'].'</td>
                            <td>'.$veranstaltungen[$i]['Vorname'].' '.$veranstaltungen[$i]['Nachname'].'</td>
                            <td>'.$veranstaltungen[$i]['veranstalter'].'</td>
                            <td>'.$veranstaltungen[$i]['teilnehmer'].'</td>
                            <td><button data-teilnahme="1" class="btn btn-success zuoderabsage" style="display:block; width:100%;">Zusagen</button></td>
                            <td><button data-teilnahme="0" class="btn btn-danger zuoderabsage" style="display:block; width:100%;">Absagen</button></td>
                        </tr>
                        '
            ;
        }
        echo '
            </tbody>
        </table>
        ';
    }
    else {
        echo '<p>Es gibt derzeit keine neuen Veranstaltungen!</p><hr>';
    }
    if($_GET['adminrechte']==1)
    {
        $query='SELECT v.v_id, v.veranstname, v.veranstaltungsdatum, v.veranstalter, v.teilnehmer, m.m_id, m.Vorname, m.Nachname FROM veranstaltungen v, mitglieder m WHERE v.m_id = m.m_id ORDER BY v.erstellerdatum DESC';
        $res1=$db->query($query);
        if(($res1->rowCount())>0)
        {
            $veranstaltungen=($res1->fetchAll(PDO::FETCH_ASSOC));
            echo '
                <style type="text/css">
                    td{
                        text-align:center;
                        vertical-align: middle;
                    }
                    th{
                        text-align:center;
                        vertical-align: middle;
                    }
                </style>
                <h3 style="text-align: center;">Ergebnisse</h3>
                <table class="table table-striped" id="bearbeitungstable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Datum</th>
                            <th>Teilnehmer</th>
                        </tr>
                    </thead>
                    <tbody>
            ';
            for($i=0; $i<$res1->rowCount(); $i++)
            {
                echo '
                        <tr data-bearbeitungs_vid="'.$veranstaltungen[$i]['v_id'].'">
                            <td>'.$veranstaltungen[$i]['veranstname'].'</td>
                            <td>'.$veranstaltungen[$i]['veranstaltungsdatum'].'</td>
                            <td>'.$veranstaltungen[$i]['teilnehmer'].'</td>
                        </tr>
                        '
                ;
            }
            echo '
            </tbody>
        </table>
        ';

            if($_GET['adminrechte']==1)
            {

            }
        }
    }
}
else {
    echo '<p>Es gibt derzeit keine neuen Veranstaltungen!</p>';
}
?>
