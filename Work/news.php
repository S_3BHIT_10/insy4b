<?php
    header('Access-Control-Allow-Origin: *');

    include("./pdo.php");
    if(isset($_GET['anzB'])) {
        $query = 'SELECT * FROM news ORDER BY erstellerdt DESC';
        $res = $db->query($query);

        $anzZeilen=$res->rowCount();
        $anzBeitraege=$_GET['anzB'];
        if($anzZeilen <= $anzBeitraege)
        {
            $zaehler=$anzZeilen;
        }
        else
        {
            $zaehler=$anzBeitraege;
        }

        if ($anzZeilen > 0) {
            $res2 = ($res->fetchAll(PDO::FETCH_ASSOC));
            for ($i = 0; $i < $zaehler; $i++) {
                echo '
                    <hr>
                    <div class="einzelN">
                        <h3>' . $res2[$i]['titel'] . '</h3>
                        <p class="verfasser"><b>' . $res2[$i]['herausgeber'] . '</b> am ' . $res2[$i]['erstellerdt'] . '</p>
                        <p>' . $res2[$i]['nachricht'] . '</p>
                    </div>
                ';
            }
        } else {
            echo '<p>Es gibt keine Neuigkeiten!</p>';
        }
    }
    else {
        echo '<p>Die Anzahl der Beiträge ist null!</p>';
    }

?>
