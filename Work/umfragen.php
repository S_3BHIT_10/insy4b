<?php
header('Access-Control-Allow-Origin: *');
include("./pdo.php");
if(isset($_GET['m_id'])&&isset($_GET['adminrechte'])) {
    $query='SELECT u_id FROM beantwUmfragen WHERE m_id ='.$_GET['m_id'];
    $res1=$db->query($query);
    $array_u_id=($res1->fetchAll(PDO::FETCH_ASSOC));
    $uids="0";
    for($i=0; $i < $res1->rowCount(); $i++)
    {
        $zwischenerg = implode(",",$array_u_id[$i]);
        $uids=$uids.",".$zwischenerg;
    }
    $query='SELECT u.u_id, u.umfrage, u.m_id, m.m_id, m.Vorname, m.Nachname FROM umfragen u, mitglieder m WHERE u.m_id = m.m_id AND u.u_id NOT IN('.$uids.')';
    $res1=$db->query($query);

    if(($res1->rowCount())>0)
    {
        $fragen=($res1->fetchAll(PDO::FETCH_ASSOC));
        echo '
                <table class="table table-striped" id="umfragentable">
            ';
        for($i=0; $i< $res1->rowCount(); $i++)
        {
            echo '
                        <tr data-uid="'.$fragen[$i]['u_id'].'"><td>
                        <h4><b>
                            '.$fragen[$i]['umfrage'].'
                        </b> - '.$fragen[$i]['Vorname'].' '.$fragen[$i]['Nachname'].'</h4>'
            ;
            $query='SELECT * FROM antworten WHERE u_id='.$fragen[$i]['u_id'];
            $res2=$db->query($query);
            if(($res2->rowCount())>0)
            {
                $antworten = ($res2->fetchAll(PDO::FETCH_ASSOC));
                for($j=0; $j<($res2->rowCount()); $j++)
                {
                    echo '
                            <button class="btn-block loginmodal-submit entscheidung" data-aid="'.$antworten[$j]['a_id'].'">'.$antworten[$j]['antwort'].'</button>
                        ';
                }
                echo ' </td></tr>';
            }
        }
        echo '
                </table>
            ';
    }
    else{
        echo '<p>Es gibt derzeit keine neuene Umfragen!</p>';
    }
    if($_GET['adminrechte']==1)
    {
        $query='SELECT u.u_id, u.umfrage, u.m_id, m.m_id, m.Vorname, m.Nachname FROM umfragen u, mitglieder m WHERE u.m_id = m.m_id';
        $res1=$db->query($query);

        if(($res1->rowCount())>0) {
            $fragen = ($res1->fetchAll(PDO::FETCH_ASSOC));
            echo '
                <hr><h3>Ergebnisse</h3>
               ';
            for ($i = 1; $i <= $res1->rowCount(); $i++) {
                //$query='SELECT antwort FROM antworten WHERE u_id ='.$fragen[$i]['u_id'];
                //echo $query;
                //$res1=$db->query($query);
                echo '
                    <table class="table table-bordered .col-md-6" style="table-layout: fixed;">
                        <tr>
                            <th colspan="'.$res1->rowCount().'">
                                ' . $fragen[$i]['umfrage'] . '
                            </th>
                        </tr>
                        <tr>';
                $query = 'SELECT * FROM antworten WHERE u_id=' . $fragen[$i]['u_id'];
                $res2 = $db->query($query);
                if (($res2->rowCount()) > 0) {
                    $antworten = ($res2->fetchAll(PDO::FETCH_ASSOC));
                    for ($j = 0; $j < ($res2->rowCount()); $j++) {
                        echo '
                            <td>' . $antworten[$j]['antwort'] . '</td>
                        ';
                    }
                    echo '</tr><tr>';
                    for ($k = 0; $k < ($res2->rowCount()); $k++) {
                        echo '
                            <td>' . $antworten[$k]['anz_klicks'] . '</td>
                        ';
                    }
                    echo '</tr>';
                }
                echo '
                    </table>
                ';
            }
        }
    }
}
else {
    echo '<p>Es gibt derzeit keine neuene Umfragen!</p>';
}
?>